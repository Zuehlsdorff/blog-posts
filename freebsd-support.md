## Native GitLab on FreeBSD
  
After nearly a year of work lets keep it short: there is now native
support for GitLab on FreeBSD. This means, that you can install it via
```pkg install www/gitlab``` and then follow the linked installation guide
for basic configuration of GitLab and its needed software.

Let me thank all of the enormeous number of people involved in this
long running project: thank you very much!

## Why native support?

Since GitLab is written in Ruby you may ask: why is native support
needed? A good question because there are already some documents
describing how to install GitLab from the source. But the instructions
are very long and changes quickly since GitLab develops constantly.
This often causes subtle failures in the process and makes it very
hard to find an still working guide.

But a port – this is how a native supported software is called in
FreeBSD – has many advantages. All dependencies were handled, a basic
working configuration is provided and many of the needed
installation-steps were done automatically.


## A short history of porting GitLab

### The Beginning

I normally face one big problem in different companies: when i try to
get a FreeBSD server a lengthy discussion is started. At the end of
the discussion there is normally one argument left: “We are not
familiar with FreeBSD and won't use it therefore”. This is quite a
stupid argument, because you will not get familiar with it when not
using it. But this is the last line you hear after all other,
especially technical, arguments fall apart. ;)

But a year ago this wasn't the case. I was stuck on one of my favorite
arguments for FreeBSD: you can have current, actual, "even" supported
third party software without sticking to a specific version. Even
without using third-party repositories or compiling everything by hand
with an high probability of rendering your server unusable and cause
problems with your package manager. FreeBSD offers you binary
packages, compiling software from source and even setup your own
binary-package repository fairly easy and transparent.
Yes, you can have Ruby 2.1, 2.2 and 2.3 on your FreeBSD,
listened in your package manager with all software still intact. Try
this on another operating system. One of the first steps in the
installation guide of GitLab is to delete the outdated Ruby and
compile a new version while ignoring the package manager. Right after
you do the same to get a current git version.

But back to history. One year ago I wanted to say: “We can even use
GitLab and replace our outdated version” - but there was no GitLab.

Since I had already experience in creating ports for FreeBSD I started
to port GitLab to FreeBSD. In the following months I discovered that
there were already multiple porting-attempts which all failed. This
knowledge would not had stopped me, but maybe I wouldn't have thought
I just would need some days to finish. ;)

### GitLab: too big, slow and fast

After verification that a source installation of an older GitLab
version works, i started to write a port for FreeBSD. A port is a
package for the package manager. Of course you can just ignore it
and compile and install everything you want, but there are multiple
advantages of having a package manager.
Rubygems for example is a package-manager itself and allows
you to have multiple versions of the same package. The disadvantage of
Rubygems is the lack of integration into the operating system. It just
did not know which packages are installed, which dependencies exists
and so on.

In FreeBSD there are numerous ports just for rubygems. Every library
has its own port, which creates a ton of work, but has several
advantages: you can use the already known package manager to update,
install or uninstall your rubygems, all dependencies to other gems or
needed software were handled and you get information about known
security issues of your gems.

But all this advantages are disadvantages for the GitLab port at the
same time. GitLab is huge. Version 8.8 needs 132 direct gems to be
installed. Adding up all their dependencies we end up with nearly 375
needed packages to build and install GitLab. Nearly a third of the
needed gems were not in the portstree when I started. And while it is
easy - compared to other UNIX-systems - to write a port it is fairly
hard to get into the official portstree. The committer keep a close
eye on high quality and it takes some time to deliver such quality in
the needed quantity.

GitLab is also very slow. Ruby applications heavily uses the
pessimistic operator for dependency definition. Translated for the
non-programmer this means, that (not only) GitLab is very, very
specific about the libraries it wants. That is a steady problem with
the current FreeBSD support, because the software provided there is
too new. The rubyteam of FreeBSD makes a very good job on keeping the
provided gems actual, but GitLab required old gems. If the old gem is
not found, it will not work, even if it could. Therefore a massive
amount of time was and is spend in convincing GitLab to run with the
current gems and test this extensively. Sometimes I just ended up with
creating an outdated version for GitLab.

And at least the GitLab project evolves very fast. It happened on a
regular basis, that my running port was already overseeded by a new
GitLab release before the extensive test-phase was done. And so a new
port was needed and because of the new requirements more tests were
also needed needed.

## The Future

But finally we have a running port. There are already active user
providing feedback or just celebrating to have it. For the future we
want to extend the support even further. User already started to bring
the currently separated documentation for FreeBSD into the project,
updating Gems in the project to reduce the patching and testing and
work on more automatic updates to stay closer to the project
releases. Our aim is to get a full support by the project itself.
